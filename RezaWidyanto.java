public class RezaWidyanto {
    public static void main(String[] args) {
        System.out.println("Nama : Reza Widyanto");
        System.out.println(" ");
        System.out.println("Deskripsi :");
        System.out.println(" Saya adalah seorang mahasiswa di Universitas indraprasta PGRI jurusan Teknik Informatika Yang baru daftar pada tahun ini, saya mengenal dunia programming pada saat SMK yang kebetulan jurusan nya adalah Rekayasa Perangkat Lunak, Namun setelah lulus di 2019 saya memilih bekerja dan tidak fokus lagi ke dunia programming, di tahun ini saya memiliki kesempatan untuk masuk kuliah yang membuat saya memilih terjun kembali ke dunia programming, kebetulan saya melihat adanya kawah edukasi yang sejalan dengan pilihan saya saat ini, tanpa fikir panjang saya coba untuk mendaftar, dan alhamdulillah sekarang saya adalah bagian dari pelatihan Back End Kawah Edukasi Batch 3, semoga semuanya di lancarkan dan jangan lupa terima kasih Kawah Edukasi");
        System.out.println(" ");
        System.out.println("Ekspetasi Mengikuti Kawah Edukasi :");
        System.out.println("Pada saat saya tahu adanya Kawah Edukasi saya seperti mendapatkan titik trang dari apa yang saya bingungkan selama ini, dengan adanya program penyaluran kerja setelah berakhirnya platihan, itu membuat rasa takut & ragu saya dalam dunia programing dapat teratasi, apalagi dengan pelatihan yang di dasari dengan dunia profesional itu membuat saya semakin percaya diri untuk berjuang di dunia programming, tinggal bagaimana saya menghadapi & memanfaatkan kesmpatan ini sebaik baik nya, sekarang saya memiliki tujuan & mimpi ingin menjadi Programmer Profesional yang bisa membuat aplikasi yang memudahkan kita semua, Terima Kasih Kawah Edukasi");
    }
}